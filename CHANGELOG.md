# SANE Standard Changes

Below you find the changes between one version of the SANE Standard to
the next in reversed chronological order, i.e most recent first.

## SANE Standard 1.06

- released as part of `sane-backends`, versions `1.0.27` thru `1.0.29`
- updates the mailing list address

**Note**: `sane-backends-1.0.26` was skipped to address a versioning
issue with its development versions.

## SANE Standard 1.05

- released as part of `sane-backends`, versions `1.0.19` thru `1.0.25`
- fixes `SANE_Parameters` member ordering to reflect existing header
  file definition
- fixes the network protocol description to use the `SANE_STATUS_GOOD`
  status instead of a non-existent `SANE_STATUS_SUCCESS`

## SANE Standard 1.04

- released as part of `sane-backends-1.0.18`
- fixes bytes per pixel equation in `sane_get_parameters()` section

## SANE Standard 1.03

- released as part of `sane-backends`, versions `1.0.9` thru `1.0.17`
- clarifies pixel value packing order for bit depth 1
- notes that bit depth 1 for frame types other than `SANE_FRAME_GRAY`
  may not be supported by frontends
- adds a large number of vendor strings and several device types
- clarifies that backend behavior is undefined unless `sane_init()`
  returns `SANE_STATUS_GOOD`
- adds `SANE_STATUS_INVAL` as possible return value for `sane_start()`
- clarifies that `sane_set_io_mode()` may be called after a call to
  `sane_start()`
- defines RPC code values for the network protocol explicitly
- updates the project's email address and website URL

## SANE Standard 1.02

- released as part of `sane-backends`, versions `1.0.4` thru `1.0.8`
- clarifies that the interpretation of sample values of bit depth 1
  for `SANE_FRAME_GRAY` is the inverse of that for other bit depths
- changes maximum length of username and password from 256 to 128
- adds an extension to the network protocol enabling the use of MD5
  digests when transmitting authentication credentials to the server
- clarifies the way network protocol requests handle authentication
  failures

**Note**: The package name was changed from `sane` to `sane-backends`
with version `1.0.4` when several frontends were split off and moved
to a new `sane-frontends` package.

## SANE Standard 1.01

- released as part of `sane-1.0.1` thru `sane-1.0.3`
- corrects the API version from `0 (draft)` to `1` in the revision
  released with `sane-1.0.3`
