# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'SANE Standard'
copyright = ''
author = 'Andreas Beck and David Mosberger'
version = 'Version 1.06'
release = version

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinxcontrib.spelling'
]

# Unconditional addition breaks 'sphinx-build'.  Versioning support is
# primarily intended for CI builds.
if 'CI' in os.environ and os.environ['CI'] == 'true':
    extensions.append('sphinxcontrib.versioning.sphinx_')

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', '.git', 'public', 'Thumbs.db', '.DS_Store']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

primary_domain = 'c'

numfig = True
numfig_format = {
    'figure': 'Figure %s',
    'table': 'Table %s',
    'section': '%s'
}

today_fmt = '2008-05-03'

highlight_language = 'c'

spelling_lang = 'en_US'
spelling_word_list_filename = 'wordlist.txt'

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_style = 'css/custom.css'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_last_updated_fmt = today_fmt
html_show_copyright = False
html_secnumber_suffix = ' '

# -- Options for PDF output --------------------------------------------------

latex_documents = [
    ('index', '%s.tex' % project.lower().replace(' ', '-'),
     project, author, 'manual', False)
]

latex_elements = {
    'papersize': 'a4paper',
    'releasename': '\!'         # suppress default with negative thin space
}
