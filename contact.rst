.. _chap:contact:

Contact Information
===================

.. index:: mailing list

The SANE standard is discussed and evolved via a mailing list. Anybody
with email access to the Internet can automatically join and leave the
discussion group by sending mail to the following address.

    mailto:sane-devel-request@alioth-lists.debian.net

To subscribe, send a mail with the body “``subscribe sane-devel``” to
the above address.

A complete list of commands supported can be obtained by sending a mail
with a subject of “``help``” to the above address. The
mailing list is archived and available through the SANE home page at
URL:

    http://sane-project.org/
